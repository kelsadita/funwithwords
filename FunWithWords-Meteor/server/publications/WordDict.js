Meteor.publish('WordDict', function () {
  return WordDict.find();
});

Meteor.publish('wordData', function(wordId) {
  return WordDict.findOne({_id: wordId});
});