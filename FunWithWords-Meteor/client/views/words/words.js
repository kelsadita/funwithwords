Template.words.helpers({
  wordSectionData: function () {
    return WordService.getWordsWithSections();
  }
});

// Services:
WordService = {
  getWordsWithSections: function () {
    return Words.find({}, {sort: {order: 1}});
  }
}

/**
*
* Word tables specific functions
*
**/

Template.wordSection.rendered = function() {

  var words = this.$('.panel-body').data('words').split(',');

  var wordTable = $('<table class="table table-bordered">');
  var wordsChunks = lodash.chunk(words, 5);

  lodash.forEach(wordsChunks, function (wordsChunk) {
    var wordsRow = $('<tr>');
    lodash.forEach(wordsChunk, function (wordItem) {

      var wordMetadata = Session.get(wordItem) || {};
      var wordCellClass = wordMetadata.cellClass || ''

      wordsRow.append('<td class="word-cell ' + wordCellClass + '" id="' + wordItem + '" data-toggle="modal" data-target="#wordDefModal">' + wordItem +  '</td>');
    });
    wordTable.append(wordsRow);
  })

  this.$('.panel-body').append(wordTable);
};

Template.wordSection.events({
  'click .word-cell': function(event) {

    // Getting clicked word
    var clickedCell = $(event.target);
    var requestedWord = clickedCell.text();

    // Updating modal context based on clicked word
    $('#wordLabel').text(lodash.capitalize(requestedWord));
    $('#wordDefModal').data('clicked-word', requestedWord);

    // Fetching word data
    var wordData = WordDict.findOne({word: requestedWord});

    // Populating the word data in the modal
    wordSectionServices.addWordData(wordData);

    // Update the difficulty button's disabling based on time elasped
    var wordMetadata = Session.get(requestedWord) || {};
    wordSectionServices.updateDifficultyDisability(wordMetadata);

    // Setting requested word in pronunciation link
    $('.pronunciation').data('word', requestedWord)
  },
});

wordSectionServices = {

  addWordData: function (wordData) {
    var definitionDiv = $('<div class="definition"><ul>');
    var exampleDiv = $('<div class="example"><ul class="word-example-list">');
    lodash.forEach(wordData.results, function (wordResult) {
      definitionDiv.append('<li><span class="label label-info">' + wordResult.partOfSpeech + '</span> ' + wordResult.definition + '</li>');
      var examples = wordResult.examples || [];
      lodash.forEach(examples, function (example) {
        exampleDiv.append('<li>' + example + '</li>');
      })
    });

    // Adding user specific examples to the list
    var userExampleList = "";
    lodash.forEach(wordData.userExample, function (userExample) {
      userExampleList += '<li>' + userExample + '</li>';
    });
    $('.my-word-example-list').html(userExampleList);

    $('.word-definition').html(definitionDiv);
    $('.word-example').html(exampleDiv);
  },

  updateDifficultyDisability: function (wordData) {

    // If no difficulty and update time available then return
    if( !wordData.difficulty || !wordData.updatedAt ) {
      $('.difficulty-button').prop( 'disabled', false );
      $('.time-remaining').html(0);
      return false;
    }

    // Get the time difference of last updated and current time
    var timeDiffernce = Math.ceil( Math.abs((new Date).getTime() - (new Date(wordData.updatedAt)).getTime()) / (1000 * 60) );

    this.applyTimeDiffAlgorithm(wordData.difficulty, timeDiffernce)
  },

  applyTimeDiffAlgorithm: function (wordDifficulty, timeDiffernce) {
    if (wordDifficulty === 'difficult') {
      this.manageDifficultyButton(timeDiffernce, 1)
    } else if(wordDifficulty === 'medium') {
      this.manageDifficultyButton(timeDiffernce, 3)
    } else if(wordDifficulty === 'easy') {
      this.manageDifficultyButton(timeDiffernce, 6)
    }
  },

  manageDifficultyButton: function (timeDiffernce, expectedTimeDiff) {

    var difficultyButton = $('.difficulty-button');

    if (timeDiffernce > expectedTimeDiff) {
      difficultyButton.prop( 'disabled', false );
      $('.time-remaining').html(0);
    } else {
      difficultyButton.prop( 'disabled', true );
      $('.time-remaining').html(expectedTimeDiff - timeDiffernce + 1);
    }
  }
}

/**
*
* Popup modal specific things
*
**/

Template.wordDataModal.events({
  'click .difficulty-button': function (event) {

    var perceivedDifficulty = $(event.target).data('difficulty');
    var wordCellClass = $(event.target).data('cell-class');
    var clickedWord = $('#wordDefModal').data('clicked-word');

    // TODO: try to find reactive way to do this
    $('#' + clickedWord).removeClass("success warning danger");
    $('#' + clickedWord).addClass(wordCellClass);

    // Setting session metadata for word.
    var wordMetadata = {
      difficulty: perceivedDifficulty,
      cellClass: wordCellClass,
      updatedAt: new Date()
    }
    Session.setPersistent(clickedWord, wordMetadata);

    // Update the difficulty button's disabling based on time elasped
    wordSectionServices.updateDifficultyDisability(wordMetadata);

    // close the modal popup
    $('.modal-close-button').click();
  },


  // Add examples to the word event
  'click .add-word-example-btn': function (event) {

    // Getting the example statement for word
    var wordExampleValue = $('.word-example-input').val();

    // Get requested word data
    var requestedWord = $('.word-title').text().toLowerCase();
    var wordData = WordDict.findOne({word: requestedWord});

    // Creating updated list of examples
    var userExamples = wordData.userExample || new Array();
    userExamples.push(wordExampleValue);

    // Updating user based word examples
    WordDict.update(wordData._id, {$set: {userExample: userExamples}});

    // Updating UI list of word examples
    $('.my-word-example-list').append('<li>' + wordExampleValue + '</li>');

    $('.word-example-input').val(''); 
  },

  'click .pronunciation': function(event) {
    event.preventDefault();
    var requestedWord = $('.pronunciation').data('word');
    new Audio('https://ssl.gstatic.com/dictionary/static/sounds/de/0/' + requestedWord + '.mp3').play();
  }
});
