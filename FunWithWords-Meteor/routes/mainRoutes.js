Router.route('/', function () {
  this.render('/words');
},
{
  waitOn: function () {
    Meteor.subscribe('WordDict');
    Meteor.subscribe('Words');
  }
});
