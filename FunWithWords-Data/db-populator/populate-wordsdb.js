var MongoClient = require('mongodb').MongoClient
, assert = require('assert')
, lazy = require('lazy')
, fs = require('fs')
, _ = require('lodash')
, worddict = {};

// Connection URL
var url = 'mongodb://localhost:3001/meteor';
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");
  generateAndSaveWordSections(db, function (wordSections) {
    insertWordSections(db, wordSections, function () {
      db.close();  
    })
  })
});

function generateAndSaveWordSections (db, callback) {
  var counter = 0;
  var wordSections = [];
  new lazy(fs.createReadStream('words.txt'))
    .lines
    .forEach(function(line){
      if (line.toString().trim() === '0') {
        return false;
      }
      
      line = line.toString();
      var lineComponents = line.split('\t');
      
      if (lineComponents.length === 1) {
        counter ++;
        //console.log(worddict);
        if (!_.isEmpty(worddict)) wordSections.push(worddict);
        worddict = createWordsObject(lineComponents[0], counter);
      } else {
        worddict.words = worddict.words.concat(lineComponents)
      }
    }).on('pipe', function () {
      // Adding last section
      wordSections.push(worddict);
      callback(wordSections);
  });
}

function createWordsObject (sectionName, order) {
    return {
      section_name: sectionName,
      order: order,
      words: []
    }
}

var insertWordSections = function(db, wordSections, callback) {
  
  // Get the documents collection
  var collection = db.collection('wordset');
  // Insert some documents
  collection.insert(wordSections, function(err, result) {
    if (err) {throw err}
    //assert.equal(err, null);
    console.log("Inserted sections");
    callback(result.ops.length);
  });
}