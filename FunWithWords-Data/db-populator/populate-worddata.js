var MongoClient = require('mongodb').MongoClient
, assert = require('assert')
, lazy = require('lazy')
, fs = require('fs')
, _ = require('lodash')
, worddict = {}
, Client = require('node-rest-client').Client
, client = new Client();

// Connection URL
var url = 'mongodb://localhost:3001/meteor';
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");
  insertWord(db, function () {
    db.close();
  });
});

var insertWord = function(db, callback) {
  
  // Get the documents collection
  var collection = db.collection('wordset');

  collection.find({}).toArray(function(err, results) {
    if (err) {throw err}
    
    var wordCollection = db.collection('worddata');

    _.forEach(results, function (result) {
      _.forEach(result.words, function (word) {
        client.get('http://www.wordsapi.com/words/' + word + '?accessToken=HroLfc1dSsXqoMLHBo5SZ4piII', function (data, response) {
          wordCollection.insert(data, function (err, data) {
            if (err) {throw err};
            console.log('Word data inserted successfully => ' + word);
          });
        })
      });
    });

    //callback();
  });
}