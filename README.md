# FunWithWords
---
An application for learning some commonly used english words (from [magoosh](http://magoosh.com/)).

# URL
---
http://funwithwords.meteor.com

**Note:** The UI takes around 10 seconds to load words. It is because the UI loads the entire word database in browser cache. Its a bug and needs to be fixed in future.

# Workflow
---
Words are arranged on the basis of their difficulty level.

* basic
* common
* advance

![Screen Shot 2015-09-19 at 10.58.57 pm.png](https://bitbucket.org/repo/8KX8L9/images/707137150-Screen%20Shot%202015-09-19%20at%2010.58.57%20pm.png)

Click on the word you want to learn and it will load its description containing following information

* All possible definitions with their forms
* Audio for pronunciation
* Example
* You can also add your own example

![Screen Shot 2015-09-19 at 11.11.57 pm.png](https://bitbucket.org/repo/8KX8L9/images/1620216253-Screen%20Shot%202015-09-19%20at%2011.11.57%20pm.png)

User can then assign difficulty level to the word and background will change accordingly.

![fun_with_words_difficulty_workflow.png](https://bitbucket.org/repo/8KX8L9/images/3564156729-fun_with_words_difficulty_workflow.png)

Once you set difficulty level you can change it only after some time period based on difficulty

![fun_with_words_time_remaining_workflow.png](https://bitbucket.org/repo/8KX8L9/images/1451261911-fun_with_words_time_remaining_workflow.png)